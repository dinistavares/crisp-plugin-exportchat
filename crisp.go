package main

import (
	"fmt"
	"log"
	"os"

	crispPluginConfig "example.com/crisp/crisp-library-plugin/plugin/config"
	crispAPI "github.com/crisp-im/go-crisp-api/crisp"
)

type crisp struct {
	config crispConfig
	client *crispAPI.Client
}

type crispConfig struct {
	crispPluginConfig.ConfigBase
}

func (crispManager *crisp) configure() {
	crispPluginConfig.Load(&crispManager.config)
}

func (crispManager *crisp) api() {
	crispManager.client = crispAPI.NewWithConfig(crispAPI.ClientConfig{RestEndpointURL: crispManager.config.CrispRESTEndpointURL, RealtimeEndpointURL: crispManager.config.CrispRealtimeEndpointURL})
}

func (crispManager *crisp) authenticate() {
	crispManager.client.AuthenticateTier("plugin", crispManager.config.CrispRESTAuthIdentifer, crispManager.config.CrispRESTAuthKey)
}

func (crispManager *crisp) events() {

	crispManager.client.Events.Listen(
		[]string{
			"message:send",
			"bucket:url:upload:generated",
		},

		func(reg *crispAPI.EventsRegister) {
			fmt.Printf("Socket is connected and is now listening.\n")

			reg.On("bucket:url:upload:generated", func(evt crispAPI.EventsReceiveBucketURLUploadGenerated) {

				fileID := *evt.ID
				identifier := *evt.Identifier
				signedURL := *evt.URL.Signed
				resourceURL := *evt.URL.Resource

				data, err := os.Open(string(fileID + ".txt"))
				if err != nil {
					log.Fatal(err)

				}

				res := putRequest(signedURL, data)

				if res.Status != "200" {
					crispManager.sendFile(resourceURL, identifier, fileID)
				}

			})
		},
		func() {
			fmt.Printf("Socket is disconnected, will try to connect again...\n")

		},
		func() {
			fmt.Printf("An error has occured with Sockets\n")
		},
	)
}

func initCrisp() *crisp {
	crispManager := &crisp{}

	crispManager.configure()
	crispManager.api()
	crispManager.authenticate()
	crispManager.events()

	return crispManager
}
